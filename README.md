# Saints Peter & Paul Cemetery Management System
- Cloud-Based Cemetary Management System
 
# Admin should be able to view information about the purchased lots and customers
- Admin will have a dashboard including:
    - Interried Information
    - Ability to add workers
    - Settings for admin and workers
    - Viewable Map
    - Financial History and Information

# Workers should be able to see a map of avalable lots in the cemetery
- Worker will have a dashboard including:
    - Map of where they need to go
    - Tasks they need to accomplish

# Client / Admin: 
- Kim Hunt

# Motivation:
- Make it easy for workers to locate avalable burial spots
- Make it efficient for the admin(Kim Hunt) to calculate financials and hold information on customers and sales.

# Technologies:
- ASP.NET + C# - Backend
- HTML + CSS - Frontend
- SQlite - Database
- Visual Studios Git - Version Control
- Docker

# Contributors:
- Clayton Littlejohn
- Akoh Atadoga
- Andrew Apatzky
- Cole Burley


## To Run the project on your local machine

1) To Run the Project using [dotnet-cli](https://dotnet.microsoft.com/download) : 

## Clone the repository:
```
$ git clone https://gitlab.com/cs4540-spring-2019/cs4540-spring-2020/cs4540_sp2020_g06.git
```

## Change your directory to the directory to the directory with the solution(.sln) file:
```
$ cd CloudCemeteryManagementSystem
```

## To run the program:
```
$ dotnet run
```

## User your browser to navigate to `http://127.0.0.1:5001` or `http://localhost:5001`:


2) To Run the Project using Docker: 
## Clone the repository:
```
$ git clone https://gitlab.com/cs4540-spring-2019/cs4540-spring-2020/cs4540_sp2020_g06.git
```

## Change your directory to the directory to the directory with the solution(.sln) file:
```
$ cd CloudCemeteryManagementSystem
```

```
$ docker-compose build
```

```
$ docker-compose up
```

## User your browser to navigate to `http://127.0.0.1:8000` or `http://localhost:8000`
## Testing File  
  
Requires: dotnet cli  
  
To run tests:  
1. clone the repo `$ git clone https://gitlab.com/cs4540-spring-2019/cs4540-spring-2020/cs4540_sp2020_g06.git` 
2. Change directory to the Test Project `cd CemetaryManagementSystem/CemetaryManagementSystem.Tests`
3. In terminal, type  `dotnet test` and hit enter  


### Sprint 10

| Name of test                                             | Status |
|----------------------------------------------------------|--------|
| Home_Controller_Index_Method_Returns_ViewType            | Pass   |
| Home_Controller_Privacy_Method_Returns_ViewType          | Pass   |
| Home_Controller_Admin_Settings_Method_Returns_ViewType   | Pass   |
| Home_Controller_Worker_Settings_Method_Returns_ViewType  | Pass   |
| Home_Controller_Add_User_Method_Returns_ViewType         | Pass   |
| Test Docker File                                         | Pass   |
| Interred_Model_Saves_To_Database                         | Pass   |
| SaveInternmentWithNoLotOwnerIdWouldRaiseException        | Pass   |
| Interred_Model_With_Invalid_Details_Saves_To_Database    | Pass   |
| Interred_Number_Starts_With_50000                        | Pass   |
| InternmentNumberFieldNotRequired                         | Pass   |
| FirstNameFieldRequired of InterredModel                  | Pass   |
| LastNameFieldRequired of InterredModel                   | Pass   |
| GenderFieldNotRequired of Interred Model                 | Pass   |
| MaritalStatusFieldNotRequired of Interred Model          | Pass   |
| NameOfSpouseFieldNotRequired of Interred Model           | Pass   |
| DateOfDeathFieldNotRequired of Interred Model            | Pass   |
| ApprovalDateFieldNotRequired of Interred Model           | Pass   |
| PlaceOfDeathFieldNotRequired of Interred Model           | Pass   |
| DateOfBurialFieldNotRequired of Interred Model           | Pass   |
| TimeOfMassFieldNotRequired of Interred Model             | Pass   |
| SectionOfCemeteryFieldNotRequired of Interred Model      | Pass   |
| PropertyStyleFieldNotRequired of Interred Model          | Pass   |
| IsVeteranFieldNotRequired of Interred Model              | Pass   |
| NameOfFuneralHomeFieldNotRequired of Interred Model      | Pass   |
| NameOfChurchFieldNotRequired of Interred Model           | Pass   |
| InformationFieldNotRequired of Interred Model            | Pass   |
| PotentialInterredFieldNotRequired of Interred Model      | Pass   |
| LastEditedByFieldNotRequired of Interred Model           | Pass   |
| LotOwnerSavesToDatabase                                  | Pass   |
| LotOwnerNumberStartsFrom12000                            | Pass   |
| LotOwnerNumberFieldNotRequired of Lot Owner Model        | Pass   |
| LotNumberFieldNotRequired of Lot Owner Model             | Pass   |
| FirstNameFieldRequired of Lot Owner Model                | Pass   |
| LastNameFieldRequired of Lot Owner Model                 | Pass   |
| AddressFieldNotRequired of Lot Owner Model               | Pass   |
| CityFieldNotRequired of Lot Owner Model                  | Pass   |
| StateFieldNotRequired of Lot Owner Model                 | Pass   |
| ZipFieldNotRequired of Lot Owner Model                   | Pass   |
| DateIssuedFieldNotRequired of Lot Owner Model            | Pass   |
| CostFieldNotRequired of Lot Owner Model                  | Pass   |
| NumberOfLotsPurchasedFieldNotRequired of Lot Owner Model | Pass   |
| LastEditedByFieldNotRequired of Lot Owner Model          | Pass   |

>  Test Output shown below
```
akohrr:CloudCemeteryManagementSystemTest akohrr$ dotnet test
Test run for /Users/akohrr/OneDrive - Bowling Green State University/spring_2020_semester/cs_5540_software_engineering_project/project_name_change/cs4540_sp2020_g06/CloudCemeteryManagementSystem/CloudCemeteryManagementSystemTest/bin/Debug/netcoreapp3.1/CloudCemeteryManagementSystemTest.dll(.NETCoreApp,Version=v3.1)
Microsoft (R) Test Execution Command Line Tool Version 16.5.0
Copyright (c) Microsoft Corporation.  All rights reserved.

Starting test execution, please wait...

A total of 1 test files matched the specified pattern.

Test Run Successful.
Total tests: 40
     Passed: 40
 Total time: 33.5401 Seconds
```

### Sprint 9

| Name of test                                            | Description                                                                                                 | Status                                                                                        | Unit Tested |
|---------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------|-------------|
| Home_Controller_Index_Method_Returns_ViewType           | This tests to confirm if the HomeController index method returns a ViewType                                 | Pass                                                                                          | Controller  |
| Home_Controller_Privacy_Method_Returns_ViewType         | This tests to confirm if the HomeController Privacy method returns a ViewType                               | Pass                                                                                          | Controller  |
| Home_Controller_Admin_Settings_Method_Returns_ViewType  | This tests to confirm if the HomeController AdminSettings method returns a ViewType                         | Pass                                                                                          | Controller  |
| Home_Controller_Worker_Settings_Method_Returns_ViewType | This tests to confirm if the HomeController WorkerSettings method returns a ViewType                        | Pass                                                                                          | Controller  |
| Home_Controller_Add_User_Method_Returns_ViewType        | This tests to confirm if the HomeController AddUser method returns a ViewType                               | Pass                                                                                          | Controller  |
| Test Docker File                                        | Test that the application can be setup using Docker                                                         | Pass                                                                                          | Devops      |
| Test Docker applies migrations to the database          | Test that database migrations have been applied to the Database in Docker before the application starts     | Pass                                                                                          | Devops      |
| Interred_Model_Saves_To_Database                        | Confirms that an Interred model with valid details will save to the database                                | Pass                                                                                          | Model       |
| SaveInternmentWithNoLotOwnerIdWouldRaiseException       | Confirms it is impossible to create an Interred without a LotOwner                                          | Pass                                                                                          | Model       |
| Interred_Model_With_Invalid_Details_Saves_To_Database   | This test confirms that an Interred model with invalid details will not save to the database                | Pass                                                                                          | Model       |
| Interred_Number_Starts_With_50000                       | This test confirms that the Internment number of an Interred model saved to the database starts with 50,000 | Pass                                                                                          | Model       |
| FirstNameFieldRequired of InterredModel                 | Confirms First name field of Interred model is required                                                     | Pass                                                                                          | Model       |
| LastNameFieldRequired of InterredModel                  | Confirms Last name field of Interred model is required                                                      | Pass                                                                                          | Model       |
| NameOfFuneralHomeFieldRequired of InterredModel                    | Confirms Name of Funeral Home field of Interred model is required                                                         | Pass | Model       |
| DateOfDeathFieldRequired of InterredModel                             | Confirms Date of Death field of Interred model is required                                                                   | Pass | Model       |
| PlaceOfDeathFieldRequired of InterredModel                          | Confirms Place of Death field of Interred model is required                                             | Pass | Model       |
| FirstNameFieldRequired of Lot Owner Model               | Confirms First Name field of Lot Owner model is required                                                    | Pass                                                                                          | Model       |
| AddressFieldRequired of Lot Owner Model                 | Confirms Address field of Lot Owner model is required                                                       | Pass                                                                                          | Model       |


>  Test Output shown below
```
akohrr:CloudCemeteryManagementSystemTest akohrr$ dotnet test
Test run for /Users/akohrr/OneDrive - Bowling Green State University/spring_2020_semester/cs_5540_software_engineering_project/project_name_change/cs4540_sp2020_g06/CloudCemeteryManagementSystem/CloudCemeteryManagementSystemTest/bin/Debug/netcoreapp3.1/CloudCemeteryManagementSystemTest.dll(.NETCoreApp,Version=v3.1)
Microsoft (R) Test Execution Command Line Tool Version 16.5.0
Copyright (c) Microsoft Corporation.  All rights reserved.

Starting test execution, please wait...

A total of 1 test files matched the specified pattern.
[xUnit.net 00:00:15.21]     CloudCemeteryManagementSystemTest.Models.LotOwnerControllerTests.LotOwnerSavesToDatabase [SKIP]
[xUnit.net 00:00:15.22]     CloudCemeteryManagementSystemTest.Models.LotOwnerControllerTests.LotOwnerNumberStartsFrom12000 [SKIP]
[xUnit.net 00:00:15.36]     CloudCemeteryManagementSystemTest.Models.InterredModelTests.SaveInternmentWithNoLotOwnerIdWouldRaiseException [SKIP]
  ! CloudCemeteryManagementSystemTest.Models.LotOwnerControllerTests.LotOwnerSavesToDatabase [1ms]
  ! CloudCemeteryManagementSystemTest.Models.LotOwnerControllerTests.LotOwnerNumberStartsFrom12000 [1ms]
  ! CloudCemeteryManagementSystemTest.Models.InterredModelTests.SaveInternmentWithNoLotOwnerIdWouldRaiseException [1ms]

Test Run Successful.
Total tests: 14
     Passed: 14
 Total time: 40.3708 Seconds
akohrr:CloudCemeteryManagementSystemTest akohrr$ 
```

---

### Sprint 8

<table>
  <tr>
    <th>Name of test</th>
    <th>Description</th>
    <th>Status</th>
    <th>Unit Tested</th>
  </tr>
  <tr>
    <td>Home_Controller_Index_Method_Returns_ViewType</td>
    <td>This tests to confirm if the HomeController index method returns a ViewType</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
  <tr>
    <td>Home_Controller_Privacy_Method_Returns_ViewType</td>
    <td>This tests to confirm if the HomeController Privacy method returns a ViewType</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
  <tr>
    <td>Home_Controller_Admin_Settings_Method_Returns_ViewType</td>
    <td>This tests to confirm if the HomeController AdminSettings method returns a ViewType</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
  <tr>
    <td>Home_Controller_Worker_Settings_Method_Returns_ViewType</td>
    <td>This tests to confirm if the HomeController WorkerSettings method returns a ViewType</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
  <tr>
    <td>Home_Controller_Add_User_Method_Returns_ViewType</td>
    <td>This tests to confirm if the HomeController AddUser method returns a ViewType</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
  <tr>
    <td>Lot_Owner_Model_With_Invalid_ZipCode_Does_Not_Save_To_Database</td>
    <td>This test confirms that a lot owner model with invalid details will not save to the database</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
  <tr>
    <td>Test Docker File</td>
    <td>Test that the application can be setup using Docker</td>
    <td>Pass</td>
    <td>Devops</td>
  </tr>
  <tr>
    <td>Test Docker applies migrations to the database</td>
    <td>Test that database migrations have been applied to the Database in Docker before the application starts</td>
    <td>Fail</td>
    <td>Devops</td>
  </tr>
  <tr>
    <td>Interred_Model_With_Valid_Details_Saves_To_Database</td>
    <td>This test confirms that an Interred model with valid details will save to the database</td>
    <td>Pass</td>
    <td>Model</td>
  </tr>
  <tr>
    <td>Interred_Model_With_Invalid_Details_Saves_To_Database</td>
    <td>This test confirms that an Interred model with invalid details will not save to the database</td>
    <td>Fail</td>
    <td>Model</td>
  </tr>
  <tr>
    <td>Interred_Number_Starts_With_50000</td>
    <td>This test confirms that the Internment number of an Interred model saved to the database starts with 50,000</td>
    <td>Pass</td>
    <td>Model</td>
  </tr>
</table>

```
akohrr:CloudCemeteryManagementSystemTest akohrr$ dotnet test
Test run for /Users/akohrr/OneDrive - Bowling Green State University/spring_2020_semester/cs_5540_software_engineering_project/project_name_change/cs4540_sp2020_g06/CloudCemeteryManagementSystem/CloudCemeteryManagementSystemTest/bin/Debug/netcoreapp3.1/CloudCemeteryManagementSystemTest.dll(.NETCoreApp,Version=v3.1)
Microsoft (R) Test Execution Command Line Tool Version 16.5.0
Copyright (c) Microsoft Corporation.  All rights reserved.

Starting test execution, please wait...

A total of 1 test files matched the specified pattern.
[xUnit.net 00:05:16.35]     CloudCemeteryManagementSystemTest.Models.LotOwnerControllerTests.Interred_Model_With_Invalid_Details_Saves_To_Database [FAIL]
  X CloudCemeteryManagementSystemTest.Models.LotOwnerControllerTests.Interred_Model_With_Invalid_Details_Saves_To_Database [35s 539ms]
  Error Message:
   Assert.Throws() Failure
Expected: typeof(Microsoft.EntityFrameworkCore.DbUpdateException)
Actual:   (No exception was thrown)
  Stack Trace:
     at CloudCemeteryManagementSystemTest.Models.LotOwnerControllerTests.Interred_Model_With_Invalid_Details_Saves_To_Database() in /Users/akohrr/OneDrive - Bowling Green State University/spring_2020_semester/cs_5540_software_engineering_project/project_name_change/cs4540_sp2020_g06/CloudCemeteryManagementSystem/CloudCemeteryManagementSystemTest/Models/LotOwnerModelTests.cs:line 210

Test Run Failed.
Total tests: 9
     Passed: 8
     Failed: 1
 Total time: 5.9230 Minutes
```

 ---

### Sprint 4

<table>
  <tr>
    <th>Name of test</th>
    <th>Description</th>
    <th>Status</th>
    <th>Unit Tested</th>
  </tr>
  <tr>
    <td>Home_Controller_Index_Method_Returns_ViewType</td>
    <td>This tests to confirm if the HomeController index method returns a ViewType</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
  <tr>
    <td>Home_Controller_Privacy_Method_Returns_ViewType</td>
    <td>This tests to confirm if the HomeController Privacy method returns a ViewType</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
  <tr>
    <td>Home_Controller_Admin_Settings_Method_Returns_ViewType</td>
    <td>This tests to confirm if the HomeController AdminSettings method returns a ViewType</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
  <tr>
    <td>Home_Controller_Worker_Settings_Method_Returns_ViewType</td>
    <td>This tests to confirm if the HomeController WorkerSettings method returns a ViewType</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
  <tr>
    <td>Home_Controller_Add_User_Method_Returns_ViewType</td>
    <td>This tests to confirm if the HomeController AddUser method returns a ViewType</td>
    <td>Pass</td>
    <td>Controller</td>
  </tr>
</table>

Output
```
akohrr:CloudCemetaryManagementSystem akohrr$ dotnet test

Test run for /Users/akohrr/OneDrive - Bowling Green State University/spring_2020_semester/cs_5540_software_engineering_project/cs4540_sp2020_g06/CloudCemetaryManagementSystem/CloudCemetaryManagementSystemTest/bin/Debug/netcoreapp3.1/CloudCemetaryManagementSystemTest.dll(.NETCoreApp,Version=v3.1)
Microsoft (R) Test Execution Command Line Tool Version 16.3.0
Copyright (c) Microsoft Corporation.  All rights reserved.

Starting test execution, please wait...

A total of 1 test files matched the specified pattern.
                                                                                                                                                                                               
Test Run Successful.
Total tests: 7
     Passed: 7
 Total time: 14.3222 Seconds
```

---

### Sprint 3  
  
<table>  
 <tr> <th>Name of test</th>  
 <th>Description</th>  
 <th>Status</th>  
 </tr> <tr> <td>LotOwner_Model_With_Valid_Details_Saves_To_Database</td>  
 <td>This tests to confirm if a LotOwner model instance is correctly, it <br>is saved to the database successfully.</td>  
 <td>Pass</td>  
 </tr> <tr> <td>LotOwner_Model_Will_Throw_Exception_If_A_Required_Field_Is_Missing</td>  
 <td>This tests confirm if a LotOwner fails to fill in a required field, an exception is thrown when you try <br>to save it to the database.</td>  
 <td>Pass</td>  
 </tr></table>  
  
Output:  
```  
akohrr:CloudCemetaryManagementSystemTest akohrr$ dotnet test  
Test run for /Users/akohrr/OneDrive - Bowling Green State University/spring_2020_semester/cs_5540_software_engineering_project/untitled folder/cs4540_sp2020_g06/CloudCemetaryManagementSystem/CloudCemetaryManagementSystemTest/bin/Debug/netcoreapp3.1/CloudCemetaryManagementSystemTest.dll(.NETCoreApp,Version=v3.1)  
Microsoft (R) Test Execution Command Line Tool Version 16.3.0  
Copyright (c) Microsoft Corporation.  All rights reserved.  
  
Starting test execution, please wait...  
  
A total of 1 test files matched the specified pattern.  
                                                                                                                                                                                
Test Run Successful.  
Total tests: 2  
     Passed: 2  
 Total time: 25.4141 Seconds  
```

---

### Sprint 2  
  
1) Unit test for View that handles the dashboard: We try to use an authenticated user to login and confirm that it does not allow the user to access the dasboard.  
The test is in UnitTest1.cs  

---
  
### Sprint 1  
  
- In sprint 1 our test goal was to set up a development enviroment and create a template for login page.  
- Frontend and Backend was running correctly through Visual Studios ASP.NET  

  
  

# Saints Peter & Paul Cemetery Management System

This is a web application created with HTML and CSS as frontend, ASP.NET and C# as backend, and SQlite for a database

## Summary of the Project 
Creates two separate main dashboards based upon who logs in - one for the Admin, and one for the workers.
Admin will be able to see and access all categories listed below, while Workers will only be able to view a map of the Cemetery.


1. Admin and Workers Dashboards - The main dashboard page for the ADMIN will include:
    - Map
    - Search Lot owner Info
    - Search Interred Info - Financials too
    - Admin & Worker Settings
    - Ability to add another worker or admin


2. Lot Owner Page (Admin Dashboard)
    - Lot ID : starts from #12,000 and goes up by 1 for each new purchase
    - Lot Owner Fields: Firstname, Lastname, Address, Number, Date Issued, Cost
    - Number of Lots purchased


3. Interred Info Page (Admin Dashboard)
    - Unique ID: starts from #50,000 and goes up by 1
    - First Name, Last Name 
        - Click on the Name to redirect to their information
    - A picture icon based on the name of the person 
        - Upload picture of marker/tombstone
        - Picture or drawing of their marker
        - Approval date
        - Received dare
        - Marker dealer (who it was brought from)
    - Dropdown Box - Sections in cemetery: 
        - BC E (baby cemetery east)
        - BCW (baby cemetery west)
        - NC A (new cemetery A)
        - NC B (new cemetery B)
        - NC C (new cemetery C)
        - NC D (new cemetery D)
        - NC E (new cemetery E)
        - NC F (new cemetery F)
        - NC G (new cemetery G)
        - NC H (new cemetery H)
        - NC I (new cemetery I)
        - OC (old cemetery)
        - OS (old section)
        - OS Memory Garden
    - Dropdown box - Property Style:
        - Burial lot or Mausoleum
    - Date of death - pop-up calendar
    - Buried date
    - Place of death
    - Mass time - Dropdown Box (ex. 8:30am, 9:00am, 9:30am...)
    - Age
    - Gender
    - Name of church
    - Name of funeral home
    - Veteran or not
    - Marital Status
    - Spouse's Name
    - Notes section at the bottom - Admin Notes
    - Created By - Bottom
    - Timestamps - Bottom


4. Ability to Run Reports on:
    - Owners
    - Intended or interred
    - Interment date
    - property style
    - burial style
    - date of death
    - gender
    - mass time
    - section
    - lot


5. Map details
    - Made using the Leaflet API
    - Map uses satellitle view tile layer from Mapbox
    - Custom markers will be able to be set
    - Map focused on Saint Paul and Peter's Cemetery in Ottawa Ohio
    - Functionality that will be added:
        - Allowing admin to set custom waypoints and display them to workers
        - Higher resolution satellite footage
        - Easily changing to other Cemeteries
        - Adding information such as tombstone pictures and interred/lot owner data


## To Run the project on your local machine

1) To Run the Project using [dotnet-cli](https://dotnet.microsoft.com/download) : 

## Clone the repository:
```
$ git clone https://gitlab.com/cs4540-spring-2019/cs4540-spring-2020/cs4540_sp2020_g06.git
```

## Change your directory to the directory to the directory with the solution(.sln) file:
```
$ cd CloudCemeteryManagementSystem
```

## To run the program:
```
$ dotnet run
```

## User your browser to navigate to `http://127.0.0.1:5001` or `http://localhost:5001`:


2) To Run the Project using Docker: 
## Clone the repository:
```
$ git clone https://gitlab.com/cs4540-spring-2019/cs4540-spring-2020/cs4540_sp2020_g06.git
```

## Change your directory to the directory to the directory with the solution(.sln) file:
```
$ cd CloudCemeteryManagementSystem
```

```
$ docker-compose build
```

```
$ docker-compose up
```

## User your browser to navigate to `http://127.0.0.1:8000` or `http://localhost:8000`:


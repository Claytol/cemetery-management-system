using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CloudCemeteryManagementSystem.Data;
using CloudCemeteryManagementSystem.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Xunit;


namespace CloudCemeteryManagementSystemTest.Models
{
    public class LotOwnerModelTests
    {
        private static IConfiguration _configuration;

        static LotOwnerModelTests()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            _configuration = builder.Build();

        }

        [Fact]
        public async Task LotOwnerSavesToDatabase()
        {
            string className = this.GetType().Name;
            string connectionString = _configuration
                .GetConnectionString("TestConnection")
                .Replace("{dbName}", className);

            var config = _configuration;

            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer(connectionString);

            using (var context = new ApplicationDbContext(optionsBuilder.Options))
            {
                await context.Database.EnsureCreatedAsync();

                await context.SaveChangesAsync();

                var lotOwner = new LotOwner()
                {
                    LotNumber = "123AAA",
                    FirstName = "Firstname",
                    LastName = "Lastname",
                    Address = "805 Lake Meno",
                    City = "Random City",
                    State = State.AK,
                    Zip = "45669",
                    DateIssued = DateTime.Today,
                    Cost = 2399,
                    NumberOfLotsPurchased = 2,
                };

                await context.AddAsync(lotOwner);

                await context.SaveChangesAsync();

            }

            using (var context = new ApplicationDbContext(optionsBuilder.Options))
            {
                var lotOwner = await context.LotOwner.FirstOrDefaultAsync(c => c.FirstName == "FirstName");
                Assert.NotNull(lotOwner);
                await context.Database.EnsureDeletedAsync();
            }
        }


        [Fact]
        public async Task LotOwnerNumberStartsFrom12000()
        {
            string className = this.GetType().Name;
            string connectionString = _configuration
                .GetConnectionString("TestConnection")
                .Replace("{dbName}", className);
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer(connectionString);

            using (var context = new ApplicationDbContext(optionsBuilder.Options))
            {

                await context.Database.EnsureCreatedAsync();


                var lotOwner = new LotOwner()
                {
                    LotNumber = "123AAA",
                    FirstName = "Firstname",
                    LastName = "Lastname",
                    Address = "805 Lake Meno",
                    City = "Random City",
                    State = State.AK,
                    Zip = "45669",
                    DateIssued = DateTime.Today,
                    Cost = 2399,
                    NumberOfLotsPurchased = 2,
                };

                await context.AddAsync(lotOwner);

                await context.SaveChangesAsync();

            }

            using (var context = new ApplicationDbContext(optionsBuilder.Options))
            {
                var lotOwner = await context.LotOwner.FirstOrDefaultAsync(c => c.FirstName == "FirstName");
                Assert.True(lotOwner.LotOwnerNumber >= 12000);
                await context.Database.EnsureDeletedAsync();
            }           
        }


        private IList<ValidationResult> ValidateModel(object model)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, ctx, validationResults, true);
            return validationResults;
        }

        [Fact]
        public void LotOwnerNumberFieldNotRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(lotOwner), v => v.MemberNames.Contains("LotOwnerNumber") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void LotNumberFieldNotRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(lotOwner), v => v.MemberNames.Contains("LotNumber") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void FirstNameFieldRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.Contains(ValidateModel(lotOwner), v => v.MemberNames.Contains("FirstName") &&
                     v.ErrorMessage.Contains("required"));
        }


        [Fact]
        public void LastNameFieldRequired()
        {
            var lotOwner = new LotOwner
            {
                ///Intentionally left empty
            };
            Assert.Contains(ValidateModel(lotOwner), v => v.MemberNames.Contains("LastName") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void AddressFieldNotRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(lotOwner), v => v.MemberNames.Contains("Address") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void CityFieldNotRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(lotOwner), v => v.MemberNames.Contains("City") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void StateFieldNotRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(lotOwner), v => v.MemberNames.Contains("State") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void ZipFieldNotRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(lotOwner), v => v.MemberNames.Contains("Zip") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void DateIssuedFieldNotRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(lotOwner), v => v.MemberNames.Contains("DateIssued") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void CostFieldNotRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(lotOwner), v => v.MemberNames.Contains("Cost") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void NumberOfLotsPurchasedFieldNotRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(lotOwner), v => v.MemberNames.Contains("NumberOfLotsPurchased") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void LastEditedByFieldNotRequired()
        {
            var lotOwner = new LotOwner
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(lotOwner), v => v.MemberNames.Contains("LastEditedBy") &&
                     v.ErrorMessage.Contains("required"));
        }

    }
}
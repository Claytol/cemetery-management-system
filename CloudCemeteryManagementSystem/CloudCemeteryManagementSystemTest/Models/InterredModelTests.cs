using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CloudCemeteryManagementSystem.Data;
using CloudCemeteryManagementSystem.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Xunit;


namespace CloudCemeteryManagementSystemTest.Models
{
    public class InterredModelTests
    {

    private static IConfiguration _configuration;
        static InterredModelTests()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            _configuration = builder.Build();

        }

        LotOwner lotOwner = new LotOwner()
        {
            LotNumber = "1ACD",
            FirstName = "FirstName",
            LastName = "LastName",
            Address = "805 Lake Meno",
            City = "Random City",
            State = State.AK,
            Zip = "45669",
            DateIssued = DateTime.Today,
            Cost = 2399,
            NumberOfLotsPurchased = 2,
        };

        Interred interred = new Interred()
        {
            FirstName = "Firstname",
            LastName = "Lastname",
            Gender = Gender.Male, //should be 1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
            MaritalStatus = MaritalStatus.Married,
            DateOfDeath = "Random city",
            PlaceOfDeath = "OH",
            DateOfBurial = DateTime.Now.Date,
            ApprovalDate = DateTime.Now.Date,
            TimeOfMass = DateTime.Now,
            SectionOfCemetery = SectionOfCemetery.BCE,
            PropertyStyle = PropertyStyle.BurialLot,
            NameOfFuneralHome = "Funeral Home",
            NameOfSpouse = "Name of Spouse",
            NameOfChurch = "Name of Church",
            Information = "Random Information",
        };

        [Fact]
        public async Task InterredModelSavesToDatabase()
        {
            string className = this.GetType().Name;
            string connectionString = _configuration
                .GetConnectionString("TestConnection")
                .Replace("{dbName}", className);
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer(connectionString);

            using (var context = new ApplicationDbContext(optionsBuilder.Options))
            {
                await context.Database.EnsureCreatedAsync();

                await context.AddAsync(lotOwner);

                await context.SaveChangesAsync();

                interred.LotOwnerId = lotOwner.Id;

                await context.AddAsync(interred);

                await context.SaveChangesAsync();

            }

            using (var context = new ApplicationDbContext(optionsBuilder.Options))
            {
                var interred = await context.Interred.FirstOrDefaultAsync(c => c.FirstName == "FirstName");
                Assert.Equal("Firstname", interred.FirstName);

                Assert.NotNull(interred);
                await context.Database.EnsureDeletedAsync();
            }
        }

        [Fact]
        public async Task SaveInternmentWithNoLotOwnerIdWouldRaiseException()
        {
            string className = this.GetType().Name;
            string connectionString = _configuration
                .GetConnectionString("TestConnection")
                .Replace("{dbName}", className);
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer(connectionString);

            using (var context = new ApplicationDbContext(optionsBuilder.Options))
            {
                await context.Database.EnsureCreatedAsync();

                await context.AddAsync(lotOwner);

                await context.SaveChangesAsync();

                //LotOwnerId of Interred model not defined intentionally

                await context.AddAsync(interred);
                await Assert.ThrowsAsync<DbUpdateException>(async () => await context.SaveChangesAsync());

            }

            using (var context = new ApplicationDbContext(optionsBuilder.Options))
            {
                await context.Database.EnsureDeletedAsync();
            }
        }


        [Fact]
        public async Task InternmentNumberStartsWith50000()
        {
            string className = this.GetType().Name;
            string connectionString = _configuration
                .GetConnectionString("TestConnection")
                .Replace("{dbName}", className);
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer(connectionString);

            using (var context = new ApplicationDbContext(optionsBuilder.Options))
            {
                await context.Database.EnsureCreatedAsync();

                await context.AddAsync(lotOwner);

                await context.SaveChangesAsync();

                interred.LotOwnerId = lotOwner.Id;

                await context.AddAsync(interred);

                await context.SaveChangesAsync();

            }

            using (var context = new ApplicationDbContext(optionsBuilder.Options))
            {
                var interred = await context.Interred.FirstOrDefaultAsync(c => c.FirstName == "FirstName");
                Assert.True(interred.InternmentNumber >= 50000);
                await context.Database.EnsureDeletedAsync();
            }
        }

        private IList<ValidationResult> ValidateModel(object model)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, ctx, validationResults, true);
            return validationResults;
        }

        [Fact]
        public void InternmentNumberFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("InternmentNumber") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void FirstNameFieldRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.Contains(ValidateModel(interred), v => v.MemberNames.Contains("FirstName") &&
                     v.ErrorMessage.Contains("required"));
        }


        [Fact]
        public void LastNameFieldRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.Contains(ValidateModel(interred), v => v.MemberNames.Contains("LastName") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void GenderFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("Gender") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void MaritalStatusFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("MaritalStatus") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void NameOfSpouseFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("NameOfSpouse") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void DateOfDeathFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("DateOfDeath") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void ApprovalDateFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("ApprovalDate") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void PlaceOfDeathFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("PlaceOfDeath") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void DateOfBurialFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("DateOfBurial") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void TimeOfMassFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("TimeOfMass") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void SectionOfCemeteryFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("SectionOfCemetery") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void PropertyStyleFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("PropertyStyle") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void IsVeteranFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("IsVeteran") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void NameOfFuneralHomeFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("NameOfFuneralHome") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void NameOfChurchFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("NameOfChurch") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void InformationFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("Information") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void PotentialInterredFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("PotentialInterred") &&
                     v.ErrorMessage.Contains("required"));
        }

        [Fact]
        public void LastEditedByFieldNotRequired()
        {
            var interred = new Interred
            {
                //Intentionally left empty
            };
            Assert.DoesNotContain(ValidateModel(interred), v => v.MemberNames.Contains("LastEditedBy") &&
                     v.ErrorMessage.Contains("required"));
        }

        

    }
}
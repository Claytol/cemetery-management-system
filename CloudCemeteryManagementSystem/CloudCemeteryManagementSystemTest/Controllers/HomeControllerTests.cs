﻿using System;
using CloudCemeteryManagementSystem.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace CloudCemeteryManagementSystemTest.Controllers
{
    public class HomeControllerTests
    {
        // public HomeControllerTests()
        // { }
       [Fact]
        public void Home_Controller_Index_Method_Returns_ViewType()
        {

            var controller = new HomeController();

            var result = controller.Index();
            var viewResult = Assert.IsType<ViewResult>(result);

        }

        [Fact]
        public void Home_Controller_Privacy_Method_Returns_ViewType()
        {

            var controller = new HomeController();

            var result = controller.Privacy();

            var viewResult = Assert.IsType<ViewResult>(result);

        }

        [Fact]
        public void Home_Controller_Admin_Settings_Method_Returns_ViewType()
        {

            var controller = new HomeController();

            var result = controller.AdminSettings();

            var viewResult = Assert.IsType<ViewResult>(result);

        }

        [Fact]
        public void Home_Controller_Worker_Settings_Method_Returns_ViewType()
        {

            var controller = new HomeController();

            var result = controller.WorkerSettings();

            var viewResult = Assert.IsType<ViewResult>(result);

        }
    }
    }

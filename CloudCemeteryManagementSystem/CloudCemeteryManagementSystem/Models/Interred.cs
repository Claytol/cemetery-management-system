using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace CloudCemeteryManagementSystem.Models
{
    public enum Gender
    {
        Male,
        Female,
        Other,
    }

    //NO MATTER WHAT, DO NOT MODIFY THE VALUES OF THE ENUM FIELDS, IT WOULD MESS WITH DATA IN THIS TABLE
    public enum SectionOfCemetery
    {
        [Display(Name = "BC E (Baby Cemetery East)")]
        BCE,
        [Display(Name = "BC W (Baby Cemetery West)")]
        BCW,
        [Display(Name = "NC A (New Cemetery A)")]
        NCA, 
        [Display(Name = "NC A (New Cemetery B)")]
        NCB,
        [Display(Name = "NC A (New Cemetery C)")]
        NCC,
        [Display(Name = "NC A (New Cemetery D)")]
        NCD,
        [Display(Name = "NC A (New Cemetery E)")]
        NCE,
        [Display(Name = "NC A (New Cemetery F)")]
        NCF,
        [Display(Name = "NC A (New Cemetery G)")]
        NCG,
        [Display(Name = "NC A (New Cemetery H)")]
        NCH,
        [Display(Name = "NC A (New Cemetery I)")]
        NCI,
        [Display(Name = "Old Cemetery")]
        OC,
        [Display(Name =  "Old Section")]
        OS,
        [Display(Name = "OS Memory Garden")]
        OSMG ,
    }

    public enum PropertyStyle
    {
        [Display(Name = "Burial Lot")]
        BurialLot,
        Mausoleum,
    }

    public enum MaritalStatus
    {
        Single,
        Married,
        Divorced,
    }

    public class Interred
    {
        public int Id { get; set; }

        public int LotOwnerId { get; set; }

        public LotOwner LotOwner { get; set; }
        

        [Display(Name = "Internment Number")]
        public int InternmentNumber { get; set; }
        
        [Required, Display(Name = "First Name")]
        public string FirstName { get; set; }
        
        [Required, Display(Name = "Last Name")]
        public string LastName { get; set; }
        
        public Gender Gender { get; set; }

        [Display(Name = "Marital Status")]
        public MaritalStatus MaritalStatus { get; set; }

        [Display(Name = "Name of Spouse")]
        public string NameOfSpouse { get; set; }
        
        [Display(Name = "Date of death"), DataType(DataType.Date)]
        public string DateOfDeath { get; set; }

        [DataType(DataType.Date)]
        public DateTime ApprovalDate { get; set; }

        [Display(Name = "Place of death")]
        public string PlaceOfDeath { get; set; }
        
        [DataType(DataType.Date), Display(Name = "Date of Burial")]
        public DateTime DateOfBurial { get; set; }
        
        [DataType(DataType.Time), Display(Name = "Time of Mass")]
        public DateTime TimeOfMass { get; set; }

        [Display(Name = "Section of Cemetery")]
        public SectionOfCemetery SectionOfCemetery { get; set; }

        [Display(Name = "Property Style")]
        public PropertyStyle PropertyStyle { get; set; }
        
        // TODO: Not sure yet if to use maps API or images
        
        // [DataType(DataType.ImageUrl)]
        // public string PictureOfTombStone { get; set; }
        
        [Display(Name = "Check the box if you are Veteran")]
        public bool IsVeteran { get; set; }
        
        [Display(Name = "Name of Funeral home")]
        public string NameOfFuneralHome { get; set; }
        
        [Display(Name = "Name of Church")]
        public string NameOfChurch { get; set; }

        public string Information { get; set; }

        [Display(Name = "Last edited by")]
        public string LastEditedBy { get; set; }

        [Display(Name = "Check the box if the person is a Potential Interred")]
        public bool PotentialInterred { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudCemeteryManagementSystem.Areas.Identity;
using Microsoft.AspNetCore.Identity.UI.V4.Pages.Account.Internal;
using RegisterModel = CloudCemeteryManagementSystem.Areas.Identity.Pages.Account.RegisterModel;

namespace CloudCemeteryManagementSystem.Models
{
    public class UserManagementModel
    {
        public List<Areas.Identity.Pages.Account.RegisterModel.InputModel> userRoleList { get; set; }
        public IEnumerable<ApplicationUser> users { get; set; }
    }
}

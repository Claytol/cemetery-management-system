using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace CloudCemeteryManagementSystem.Models
{
        public enum State
    {
        [Display(Name = "Alabama")]
        AL,
        [Display(Name = "Alaska")]
        AK,
        [Display(Name = "Arkansas")]
        AR,
        [Display(Name = "Arizona")]
        AZ,
        [Display(Name = "California")]
        CA,
        [Display(Name = "Colorado")]
        CO,
        [Display(Name = "Connecticut")]
        CT,
        [Display(Name = "Delaware")]
        DE,
        [Display(Name = "Florida")]
        FL,
        [Display(Name = "Georgia")]
        GA,
        [Display(Name = "Hawaii")]
        HI,
        [Display(Name = "Iowa")]
        IA,
        [Display(Name = "Idaho")]
        ID,
        [Display(Name = "Illinois")]
        IL,
        [Display(Name = "Indiana")]
        IN,
        [Display(Name = "Kansas")]
        KS,
        [Display(Name = "Kentucky")]
        KY,
        [Display(Name = "Louisiana")]
        LA,
        [Display(Name = "Massachusetts")]
        MA,
        [Display(Name = "Maryland")]
        MD,
        [Display(Name = "Maine")]
        ME,
        [Display(Name = "Michigan")]
        MI,
        [Display(Name = "Minnesota")]
        MN,
        [Display(Name = "Missouri")]
        MO,
        [Display(Name = "Mississippi")]
        MS,
        [Display(Name = "Montana")]
        MT,
        [Display(Name = "North Carolina")]
        NC,
        [Display(Name = "North Dakota")]
        ND,
        [Display(Name = "Nebraska")]
        NE,
        [Display(Name = "New Hampshire")]
        NH,
        [Display(Name = "New Jersey")]
        NJ,
        [Display(Name = "New Mexico")]
        NM,
        [Display(Name = "Nevada")]
        NV,
        [Display(Name = "New York")]
        NY,
        [Display(Name = "Oklahoma")]
        OK,
        [Display(Name = "Ohio")]
        OH,
        [Display(Name = "Oregon")]
        OR,
        [Display(Name = "Pennsylvania")]
        PA,
        [Display(Name = "Rhode Island")]
        RI,
        [Display(Name = "South Carolina")]
        SC,
        [Display(Name = "South Dakota")]
        SD,
        [Display(Name = "Tennessee")]
        TN,
        [Display(Name = "Texas")]
        TX,
        [Display(Name = "Utah")]
        UT,
        [Display(Name = "Virginia")]
        VA,
        [Display(Name = "Vermont")]
        VT,
        [Display(Name = "Washington")]
        WA,
        [Display(Name = "Wisconsin")]
        WI,
        [Display(Name = "West Virginia")]
        WV,
        [Display(Name = "Wyoming")]
        WY

    }
    public class LotOwner
    {
        public int Id { get; set; }

        [Display(Name = "Lot Owner Number")]
        public int LotOwnerNumber { get; set; }

        [Display(Name = "Lot Number")]
        public string LotNumber { get; set; }
        
        [Required, Display(Name = "First Name")]
        public string FirstName { get; set; }
        
        [Required, Display(Name = "Last Name")]
        public string LastName { get; set; }
        
        public string Address { get; set; }

        public string City { get; set; }

        public State State { get; set; }
        
        [RegularExpression(@"^\d{5}(-\d{4})?$", ErrorMessage = "Invalid Zip")]
        public string Zip { get; set; }
        
        [DataType(DataType.Date), Display(Name = "Date Issued")]
        public DateTime DateIssued { get; set; }
        
        public float Cost { get; set; }
        
        [Display(Name = "Lots Purchased")]
        public int NumberOfLotsPurchased { get; set; }

        public List<Interred> Interreds { get; set; }
        
        [Display(Name = "Last Edited by")]
        public string LastEditedBy { get; set; }

    }
}

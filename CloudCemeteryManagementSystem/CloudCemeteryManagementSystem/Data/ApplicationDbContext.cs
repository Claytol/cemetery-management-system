﻿using System;
using System.Collections.Generic;
using System.Text;
using CloudCemeteryManagementSystem.Areas.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using CloudCemeteryManagementSystem.Models;

namespace CloudCemeteryManagementSystem.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        
        public DbSet<LotOwner> LotOwner { get; set; }
        
        public DbSet<Interred> Interred { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.HasSequence<int>("InternmentNumber")
                .StartsAt(50000)
                .IncrementsBy(1);

            modelBuilder.Entity<Interred>()
                .Property(o => o.InternmentNumber)
                .HasDefaultValueSql("NEXT VALUE FOR dbo.InternmentNumber");

            modelBuilder.HasSequence<int>("LotOwnerNumber")
                .StartsAt(12000)
                .IncrementsBy(1);

            modelBuilder.Entity<LotOwner>()
                .Property(o => o.LotOwnerNumber)
                .HasDefaultValueSql("NEXT VALUE FOR dbo.LotOwnerNumber");

            modelBuilder.Entity<Interred>()
                .Property( o => o.Gender)
                .HasConversion(
                    o => o.ToString(),
                    o => (Gender)Enum.Parse(typeof(Gender), o));

            modelBuilder.Entity<Interred>()
                .Property( o => o.SectionOfCemetery)
                .HasConversion(
                    o => o.ToString(),
                    o => (SectionOfCemetery)Enum.Parse(typeof(SectionOfCemetery), o));

            modelBuilder.Entity<Interred>()
                .Property( o => o.PropertyStyle)
                .HasConversion(
                    o => o.ToString(),
                    o => (PropertyStyle)Enum.Parse(typeof(PropertyStyle), o));

            modelBuilder.Entity<Interred>()
                .Property( o => o.MaritalStatus)
                .HasConversion(
                    o => o.ToString(),
                    o => (MaritalStatus)Enum.Parse(typeof(MaritalStatus), o));

            modelBuilder.Entity<LotOwner>()
                .Property( o => o.State)
                .HasConversion(
                    o => o.ToString(),
                    o => (State)Enum.Parse(typeof(State), o));

        }
    }
}
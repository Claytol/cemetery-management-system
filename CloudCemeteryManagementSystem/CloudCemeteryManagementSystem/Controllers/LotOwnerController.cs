using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CloudCemeteryManagementSystem.Data;
using CloudCemeteryManagementSystem.Models;

namespace CloudCemeteryManagementSystem.Controllers
{
    public class LotOwnerController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LotOwnerController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: LotOwner
        public async Task<IActionResult> Index()
        {
            return View(await _context.LotOwner.ToListAsync());
        }
        

        // GET: LotOwner/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lotOwner = await _context.LotOwner
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lotOwner == null)
            {
                return NotFound();
            }

            return View(lotOwner);
        }

        // GET: LotOwner/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LotOwner/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,LotOwnerNumber,LotNumber,FirstName,LastName,Address,City,State,Zip,DateIssued,Cost,NumberOfLotsPurchased")] LotOwner lotOwner)
        {
            if (ModelState.IsValid)
            {
                lotOwner.LastEditedBy = User.FindFirstValue(ClaimTypes.Name);
                _context.Add(lotOwner);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(lotOwner);
        }

        // GET: LotOwner/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lotOwner = await _context.LotOwner.FindAsync(id);
            if (lotOwner == null)
            {
                return NotFound();
            }
            return View(lotOwner);
        }

        // POST: LotOwner/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,LotOwnerNumber,LotNumber,FirstName,LastName,Address,City,State,Zip,DateIssued,Cost,NumberOfLotsPurchased")] LotOwner lotOwner)
        {
            if (id != lotOwner.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lotOwner);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LotOwnerExists(lotOwner.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(lotOwner);
        }

        // GET: LotOwner/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lotOwner = await _context.LotOwner
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lotOwner == null)
            {
                return NotFound();
            }

            return View(lotOwner);
        }

        // POST: LotOwner/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lotOwner = await _context.LotOwner.FindAsync(id);
            _context.LotOwner.Remove(lotOwner);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LotOwnerExists(int id)
        {
            return _context.LotOwner.Any(e => e.Id == id);
        }
    }
}

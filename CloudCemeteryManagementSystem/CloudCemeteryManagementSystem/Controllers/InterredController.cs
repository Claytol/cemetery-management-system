using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CloudCemeteryManagementSystem.Data;
using CloudCemeteryManagementSystem.Models;

namespace CloudCemeteryManagementSystem.Controllers
{
    public class InterredController : Controller
    {
        private readonly ApplicationDbContext _context;

        public InterredController(ApplicationDbContext context)
        {
            _context = context;
        }


        public async Task<IActionResult> Index(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lotOwner =  await _context.LotOwner
                .Where(m => m.Id == id).Include(m => m.Interreds)
                .ToListAsync();

            // if a lotOwner does not exist with the Id it return NotFound
            bool isEmpty = !lotOwner.Any();
            if (isEmpty)
            {
                return NotFound();
            }

            return View(lotOwner);
        }

        // GET: Interred/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var interred = await _context.Interred
                .FirstOrDefaultAsync(m => m.Id == id);
            if (interred == null)
            {
                return NotFound();
            }

            return View(interred);
        }

        // GET: Interred/Create/LotOwner/1
        public async Task<IActionResult> Create(int? id)
        {
            var lotOwnerId = id;
            if (lotOwnerId == null)
            {
                return NotFound();
            }

            var lotOwner = await _context.LotOwner.FindAsync(lotOwnerId);
            if (lotOwner == null)
            {
                return NotFound();
            }
            ViewBag.LotOwner = lotOwner;
            return View();
        }

        // POST: Interred/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind(
            "LotOwnerId,FirstName,LastName,DateOfDeath,PlaceOfDeath,PropertyStyle,Gender"+
            "DateOfBurial,ApprovalDate,TimeOfMass,MaritalStatus,SectionOfCemetery,"+
            "IsVeteran,NameOfFuneralHome,NameOfSpouse,NameOfChurch,Information")] Interred interred)
        {
            if (ModelState.IsValid)
            {
                interred.LastEditedBy = User.FindFirstValue(ClaimTypes.Name);
                _context.Add(interred);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new{id = interred.LotOwnerId});
            }
            return View(interred);
        }

        // GET: Interred/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var interred = await _context.Interred.FindAsync(id);
            if (interred == null)
            {
                return NotFound();
            }
            ViewData["LotOwnerId"] = new SelectList(_context.Set<LotOwner>(), "LotOwnerId", "LotOwnerId", interred.LotOwnerId);
            return View(interred);
        }

        // POST: Interred/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind(
            "Id,LotOwnerId,InternmentNumber,FirstName,LastName,DateOfDeath,PlaceOfDeath,PropertyStyle,Gender"+
            "DateOfBurial,ApprovalDate,TimeOfMass,MaritalStatus,SectionOfCemetery,"+
            "IsVeteran,NameOfFuneralHome,NameOfSpouse,NameOfChurch,Information")] Interred interred)
        {
            if (id != interred.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(interred);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InterredExists(interred.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new{id = interred.LotOwnerId});
            }
            ViewData["LotOwnerId"] = new SelectList(_context.Set<LotOwner>(), "LotOwnerId", "LotOwnerId", interred.LotOwnerId);
            return View(interred);
        }

        // GET: Interred/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var interred = await _context.Interred
                .FirstOrDefaultAsync(m => m.Id == id);
            if (interred == null)
            {
                return NotFound();
            }

            return View(interred);
        }

        // POST: Interred/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var interred = await _context.Interred.FindAsync(id);
            _context.Interred.Remove(interred);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new{id = interred.LotOwnerId});
        }

        private bool InterredExists(int id)
        {
            return _context.Interred.Any(e => e.Id == id);
        }
    }
}

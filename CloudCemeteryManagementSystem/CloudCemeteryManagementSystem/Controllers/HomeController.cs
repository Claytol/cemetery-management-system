﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CloudCemeteryManagementSystem.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CloudCemeteryManagementSystem.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController()//ILogger<HomeController> logger)
        {
            //_logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult SearchLotOwnerInfo()
        {
            ViewData["Message"] = "Your Dashboard.";

            return View();
        }

        public IActionResult SearchInterredInfo()
        {
            ViewData["Message"] = "Your Dashboard.";

            return View();
        }

        public IActionResult Map()
        {
            ViewData["Message"] = "Your Dashboard.";

            return View();
        }

        [HttpPost]
        public ActionResult StoreMarkers(string latlng)
        {
            int start = latlng.IndexOf("(");
            int end = latlng.IndexOf(")");
            int length = end - start;
            string parsedString = latlng.Substring(start + 1, length - 1);
            
            
            
            return View("Map");
        }

        public IActionResult AdminSettings()
        {
            ViewData["Message"] = "Your Dashboard.";

            return View();
        }

        public IActionResult WorkerSettings()
        {
            ViewData["Message"] = "Your Dashboard.";

            return View();
        }
        

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        public IActionResult Dashboard()
        {
            if (User.IsInRole("Administrator"))
            {
                return View("AdminDashboard");
            }

            if (User.IsInRole("Worker"))
            {
                return View("WorkerDashboardPlaceHolder");
            }

            return View("ErrorPleaseSignIn");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using CloudCemeteryManagementSystem.Data;
using CloudCemeteryManagementSystem.Areas.Identity.Pages.Account;
using CloudCemeteryManagementSystem.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CloudCemeteryManagementSystem.Areas.Identity;

namespace CloudCemeteryManagementSystem.Controllers
{
    public class UserManagementController : Controller
    {
        private static UserManager<ApplicationUser> userManager;
        private List<RegisterModel.InputModel> Input = new List<RegisterModel.InputModel>();

        public UserManagementController(RoleManager<IdentityRole> roleMgr, UserManager<ApplicationUser> userMgr)
        {
            userManager = userMgr;
        }
        
        [HttpPost]
        public async Task<IActionResult> Delete(string Id)
        {
            ApplicationUser user = await userManager.FindByIdAsync(Id);
            if (user != null)
            {
                IdentityResult result = await userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    Errors(result);
                }
            }
            else
            {
                ModelState.AddModelError("", "No role found");
            }

            return View("Index", new UserManagementModel
            {
                userRoleList = Input,
                users = userManager.Users
            });
        }

        //[Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Index()
        {

            var users = await userManager.Users.ToListAsync();
            RegisterModel.InputModel inputModel = new RegisterModel.InputModel();

            foreach (var v in users)
            {
                inputModel = new RegisterModel.InputModel();
                var roles = await userManager.GetRolesAsync(v);
                inputModel.Email = v.UserName;
                inputModel.FirstName = v.FirstName;
                inputModel.LastName = v.LastName;
                inputModel.role = "No Role";
                inputModel.RegisteredBy = v.RegisteredBy;
                inputModel.Id = v.Id;
                foreach (var r in roles)
                {
                    if (!inputModel.role.Contains(","))
                    {
                        inputModel.role = r;
                    }
                    else
                    {
                        inputModel.role = "," + r;
                    }
                }
                Input.Add(inputModel);
            }
            return View("Index",new UserManagementModel
            {
               userRoleList = Input
            });
        }

        private void Errors(IdentityResult result)
        {
            foreach (IdentityError error in result.Errors)
                ModelState.AddModelError("", error.Description);
        }
    }


}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CloudCemeteryManagementSystem.Migrations
{
    public partial class InterredForeignKeyFieldToLotOwner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LotOwnerId",
                table: "Interred",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Interred_LotOwnerId",
                table: "Interred",
                column: "LotOwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Interred_LotOwner_LotOwnerId",
                table: "Interred",
                column: "LotOwnerId",
                principalTable: "LotOwner",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Interred_LotOwner_LotOwnerId",
                table: "Interred");

            migrationBuilder.DropIndex(
                name: "IX_Interred_LotOwnerId",
                table: "Interred");

            migrationBuilder.DropColumn(
                name: "LotOwnerId",
                table: "Interred");
        }
    }
}

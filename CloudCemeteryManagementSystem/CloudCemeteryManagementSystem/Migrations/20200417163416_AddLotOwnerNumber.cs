﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CloudCemeteryManagementSystem.Migrations
{
    public partial class AddLotOwnerNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence<int>(
                name: "LotOwnerNumber",
                startValue: 12000L);

            migrationBuilder.AddColumn<int>(
                name: "LotOwnerNumber",
                table: "LotOwner",
                nullable: false,
                defaultValueSql: "NEXT VALUE FOR dbo.LotOwnerNumber");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropSequence(
                name: "LotOwnerNumber");

            migrationBuilder.DropColumn(
                name: "LotOwnerNumber",
                table: "LotOwner");
        }
    }
}

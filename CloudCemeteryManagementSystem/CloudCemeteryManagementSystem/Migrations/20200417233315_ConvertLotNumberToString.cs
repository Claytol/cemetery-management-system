﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CloudCemeteryManagementSystem.Migrations
{
    public partial class ConvertLotNumberToString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Lot",
                table: "LotOwner");

            migrationBuilder.AddColumn<string>(
                name: "LotNumber",
                table: "LotOwner",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LotNumber",
                table: "LotOwner");

            migrationBuilder.AddColumn<int>(
                name: "Lot",
                table: "LotOwner",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CloudCemeteryManagementSystem.Migrations
{
    public partial class AddRegisteredBy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RegisteredBy",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RegisteredBy",
                table: "AspNetUsers");
        }
    }
}

using Microsoft.AspNetCore.Identity;

namespace CloudCemeteryManagementSystem.Areas.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        public string RegisteredBy { get; set; }
    }
}